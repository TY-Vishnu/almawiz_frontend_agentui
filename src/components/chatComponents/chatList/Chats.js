import React from 'react'
import { Figure } from 'react-bootstrap'
import './ChatList.css'
import { Link } from 'react-router-dom'
import profile from './agent.jpg';
import endUser from './endUser.png';

const Chats = ({ user, onClick }) => {

    return (
        // <Link to={`/conversations/${user.conversation.id}`}>
        <div className="conversation " onClick={() => onClick(user.conversation.id)} >


            <img src={endUser} alt="" />

            {/* <img src={user.conversation.assignee.thumbnail} alt="" /> */}
            <div className="title-text">{user.sender.name}</div>
            <div className="created-date">{user.conversation.createdAt}</div>
            <div className="conversation-message">{user.conversation.lastmessage}</div>





        </div>
        // </Link>
    )
}

// Chats.defaultProps = {
//     user.conversation.assignee.thumbnail: endUser
// }

export default Chats
// onClick={() => onClick(user.id)}