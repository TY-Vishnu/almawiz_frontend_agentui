import React, { useState, useEffect } from 'react'
import Chats from './Chats'
import './ChatList.css'
import { ListGroup } from 'react-bootstrap'

const ChatList = ({ userChats, onClick, conversationId, allData }) => {
    useEffect(() => {
        console.log('mounted Chat List')
    })



    // const [activeKey, setActiveKey] = useState(userChats.id)
    return (


        // <div>
        //     {userChats.map((chats) => (
        //         <div key={chats.id}>{chats.name}</div>
        //     ))}
        // </div>

        <ListGroup variant="flush" style={{ height: '80vh' }}>
            {allData.map((conversation, index) => (
                <div
                    key={index}
                    action
                    style={{ background: '#ffffff' }}
                    active={conversation.conversation.id === conversationId}
                >
                    {<Chats key={conversation.conversation.id} user={conversation} onClick={onClick} />}
                </div>
            ))}
        </ListGroup>




        // <div id="conversation-list" activeKey={activeKey} onSelect={setActiveKey}>

        //     { userChats.map((userChat) => (
        //         <Chats key={userChat.id} eventKey={userChat.id} user={userChat} onClick={onClick} />
        //     ))}
        // </div>


    )
}

export default ChatList
