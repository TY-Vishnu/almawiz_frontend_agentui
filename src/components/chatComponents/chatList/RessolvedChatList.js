import React from 'react'
import { ListGroup, ListGroupItem } from 'react-bootstrap'
import './ChatList.css'
import Chats from './Chats'

const UnressolvedChatlist = ({ ressolvedData, onClick }) => {
    return (
        <ListGroup variant="flush" style={{ height: '80vh' }}>
            {ressolvedData.map((conversation, index) => (
                <div
                    key={index}
                    action
                    style={{ background: '#ffffff' }}

                >
                    <Chats key={conversation.id} eventKey={conversation.id} user={conversation} onClick={onClick} />
                </div>
            ))}
        </ListGroup>
    )
}

export default UnressolvedChatlist
