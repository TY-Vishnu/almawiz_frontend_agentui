import React from 'react'
import profilePik from '../chatList/agent.jpg'
import './AddProp.css'
import { Card, ListGroupItem, ListGroup } from 'react-bootstrap'
import { faVideo, faPhoneSquareAlt } from '@fortawesome/free-solid-svg-icons'
import { useDetails } from '../../details/DetailsProvider'
import { useLogin } from '../../details/LoginProvider'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const AddProp = ({ userData }) => {
    const { details } = useDetails();
    const { login } = useLogin();
    return (
        <div>
            <Card
                bg={'light'}
                text={'dark'}
                className='add-details'>
                <Card.Header><FontAwesomeIcon icon={faVideo} ></FontAwesomeIcon></Card.Header>
                <Card.Header><FontAwesomeIcon icon={faPhoneSquareAlt} ></FontAwesomeIcon></Card.Header>
                {/* <Card.Img variant="top" src={profilePik} /> */}
                <Card.Body>
                    <Card.Title>{userData}</Card.Title>
                    <Card.Text>

                        <ListGroup className="list-group-flush">
                            {details.map((data) => (
                                <ListGroup.Item key={data.id}>{ }</ListGroup.Item>
                            ))}

                        </ListGroup>
                        <ListGroup className="list-group-flush">
                            {login.map((log) => (
                                <ListGroup.Item key={log.email}>{log.email}</ListGroup.Item>
                            ))}

                        </ListGroup>
                    </Card.Text>
                </Card.Body>
                {/* <Card.Body>
                    <Card.Link href="#">Card Link</Card.Link>
                    <Card.Link href="#">Another Link</Card.Link>
                </Card.Body> */}
            </Card>
        </div>
    )
}

export default AddProp
