import React, { useState } from 'react'
import { ButtonGroup, Button, Nav, Tab, Tabs, Spinner } from 'react-bootstrap'
import ChatList from '../chatList/ChatList'
import RessolvedChatList from '../chatList/RessolvedChatList'
import UnressolvedChatlist from '../chatList/UnressolvedChatlist'
import './sideBar.css'

const RESSOLVED_ID = 'ressolved'
const UNRESSOLVED_ID = 'unressolved'
const ALL_ID = 'all'
const Sidebar = ({ userChats, onClick, conversationId, unRessolvedData, ressolvedData, allData, chatLoading, resolvedLoading, unresolvedLoading }) => {
    const [activeKey, setActiveKey] = useState(ALL_ID)
    // const conversationsOpen = activeKey === CONVERSATIONS_KEY

    return (
        <div style={{ width: '100%', transform: 'translate(0px, -38px)' }} className="d-flex flex-column ">

            <Tab.Container activeKey={activeKey} onSelect={setActiveKey}>


                <Nav variant="" className="justify-content-left nav-change">
                    <Nav.Item className="nav-change">
                        <Nav.Link className="nav-text-change" eventKey={RESSOLVED_ID}>Ressolved</Nav.Link>
                    </Nav.Item>
                    <Nav.Item className="nav-change" >
                        <Nav.Link className="nav-text-change" eventKey={UNRESSOLVED_ID}>Unressolved</Nav.Link>
                    </Nav.Item>
                    <Nav.Item className="nav-change">
                        <Nav.Link className="nav-text-change" eventKey={ALL_ID}>All</Nav.Link>
                    </Nav.Item>
                </Nav>
                <Tab.Content className="border-right overflow-auto flex-grow-1">
                    <Tab.Pane eventKey={RESSOLVED_ID}>
                        {resolvedLoading ? <RessolvedChatList ressolvedData={ressolvedData} onClick={onClick} /> :
                            <Button variant="primary" disabled>
                                <Spinner
                                    as="span"
                                    animation="grow"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                />
Loading Resolved Chat List...
</Button>
                        }
                    </Tab.Pane>
                    <Tab.Pane eventKey={UNRESSOLVED_ID}>
                        {unresolvedLoading ? <UnressolvedChatlist onClick={onClick} unRessolvedData={unRessolvedData} /> :
                            <Button variant="primary" disabled>
                                <Spinner
                                    as="span"
                                    animation="grow"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                />
Loading Unressolved Chat List...
</Button>}

                    </Tab.Pane>
                    <Tab.Pane eventKey={ALL_ID}>

                        <ChatList userChats={userChats} allData={allData} onClick={onClick} conversationId={conversationId} /> :

                    </Tab.Pane>
                </Tab.Content>
            </Tab.Container>
            {/* <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
                <Tab eventKey="home" title="Home">
                    <ChatList userChats={userChats} onClick={onClick} />
                </Tab>
                <Tab eventKey="profile" title="Profile">
                    {userChats.map(chats => (<div>{chats.name}</div>))}
                </Tab>
                <Tab eventKey="contact" title="Contact" disabled>
                </Tab>
            </Tabs> */}
            {/* <ButtonGroup className="mb-2" activeKey={activeKey} onSelect={setActiveKey} style={{ minWidth: "-webkit-fill-available", position: "relative", top: "-38px" }}>
                <Button variant="info" eventKey={RESSOLVED_ID}>Ressolved</Button>
                <Button variant="info" eventKey={UNRESSOLVED_ID}>Unressolved</Button>
                <Button variant="info" eventKey={ALL_ID}>All</Button>
            </ButtonGroup> */}
        </div>
    )
}

export default Sidebar
