import React from 'react'
import { Navbar, Button, Dropdown, DropdownButton, Badge } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import { useState, useEffect } from 'react'
import './Header.css'
// import agent from './AgentPP.jpg';
import agent from './agent.png'
import axios from 'axios'
import LogOut from '../../pages/loginPage/LogOut';

const Header = ({ agentData, agentId, conversationId }) => {
    const [changeStatus, setChangeStatus] = useState('online')
    const [offline, setOffline] = useState(false)
    const [statusColor, setStatusColor] = useState(1)
    const status = [
        {
            id: 1,
            currentStatus: 'online',
        },
        {
            id: 2,
            currentStatus: 'offline',
        },
        {
            id: 3,
            currentStatus: 'busy',
        }
    ];
    const handleStatusChange = (index) => {
        const color = status.filter((statid) => (statid.id === index))
        setStatusColor(color[0].id)
        console.log("current status", color[0].currentStatus);
        setChangeStatus(color[0].currentStatus)
    }
    useEffect(() => {


        const patchStatus = setInterval(() => {
            // debugger
            axios.patch(`https://chat-server-pritam.herokuapp.com/account/${agentId}/status`,
                { availability_status: changeStatus }
            ).then((res) => {
                console.log(res);
            }).catch(error => {
                console.log(error);
            })
            return clearInterval(patchStatus)
        }, 5000)
    }, [])




    return (
        <div className="agent-nav-bar">

            <Navbar bg="light" variant="dark"  >
                <Navbar.Brand href="#home">
                    <span class="dot" style={statusColor === 1 ? { 'backgroundColor ': 'green' } : statusColor === 2 ? { 'backgroundColor': 'red' } : { 'backgroundColor': 'orange' }}></span>
                    <img
                        alt=""
                        src={agent}
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                        style={{
                            borderRadius: "100%",
                        }}
                    />{' '}
                    <div className="agent-name"
                    >{agentData.available_name}</div>
                    {/* agentData.user.available_name */}

                </Navbar.Brand>
            </Navbar>

            <Dropdown className="float-right" style={{
                top: "-46px",
                left: "-7px"
            }}>
                {/* <Button variant="success">Split Button</Button> */}

                <Dropdown.Toggle split variant="#1f93ff" style={{ background: "#3fa3ff" }} id="dropdown-split-basic" />

                <Dropdown.Menu className="dropdown">
                    <Dropdown.Item href="">Profile Setting</Dropdown.Item>


                    <Dropdown href="" id="dropdown-split-basic">Change Status{status.map((stat) => (<Dropdown.Item eventKey="1" key={stat.id} onClick={() => handleStatusChange(stat.id)} >{stat.currentStatus}</Dropdown.Item>))}</Dropdown>
                    {/* <Link to='/' onClick={() => localStorage.clear()}><Dropdown.Item>Logout</Dropdown.Item></Link > */}
                    <Dropdown.Item><LogOut /></Dropdown.Item>

                </Dropdown.Menu>
            </Dropdown>
        </div >
    )
}

export default Header
