import React from 'react';
import { Button, Navbar, Nav, Form } from 'react-bootstrap';
import { useState, useRef, useEffect, useCallback } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons'
import axios from 'axios';
import './chatBox.css'
import userimg from '../../chatComponents/chatList/endUser.png';
const ChatBox = ({ inbox, onAdd, ressolvedData, conversationsId, data, agentId, allData }) => {
    var today = new Date();
    var time = today.getHours() + ":" + today.getMinutes();
    const [toggle, setToggle] = useState(true);
    const [sentMessage, setsentMessage] = useState('');
    const [changeToggle, setChangeToggle] = useState('');
    const [sentTime, setSentTime] = useState(time);
    const [senderName, setSenderName] = useState('');
    const [message_id, setMessage_id] = useState('agent1');
    // const lastMessageRef = useRef(null);
    const setRef = useCallback(node => {
        if (node) {
            node.scrollIntoView({ smooth: true })
        }
    }, []);



    const ressolveHandle = () => {
        if (toggle === true) {
            setToggle(false);
            setChangeToggle('resolved');
        } else if (toggle === false) {
            setToggle(true);
            setChangeToggle('open');

        }
        console.log(toggle);
    }
    useEffect(() => {
        // debugger
        const toggleRessolve = setInterval(() => {
            axios.post(`https://chat-server-pritam.herokuapp.com/account/${agentId}/conversations/${conversationsId}/toggle`,
                { conversationstatus: changeToggle }
            ).then((res) => {
                // debugger
                console.log('status ressolved', res);
            }).catch((err) => {
                console.log(err);
            })
            return clearInterval(toggleRessolve)
        }, 1000)
    })



    // useEffect(() =>{
    //     setsentTime(time);
    // },[])

    const addMessage = (e) => {
        e.preventDefault();
        var today = new Date();
        var time = today.getHours() + ":" + today.getMinutes();
        setSentTime(time)
        onAdd(sentMessage);
        setsentMessage('')
        console.log(inbox);
        // sentTime('')
        // console.log('sent time', sentTime);
        console.log('new messages added', sentMessage);
        // console.log("inbosId", inbox_id);
    }


    return (
        <>
            <Navbar bg="light" variant="tabs" className="nav-bar">
                <img
                    alt=""
                    src={userimg}
                    width="40"
                    height="40"
                    className="d-inline-block align-top"
                    style={{ borderRadius: "100%" }}
                />
                {
                    ressolvedData.map(nameData => nameData.conversation.id === conversationsId && <Navbar.Brand href="" style={{ color: "#3c4858" }}>{nameData.sender.name}</Navbar.Brand>)

                }
                {allData.map(nameData => nameData.conversation.id === conversationsId && <Navbar.Brand href="" style={{ color: "#3c4858" }}>{nameData.sender.name}</Navbar.Brand>)}

                <Nav className="mr-auto">
                    <Nav.Link href=""></Nav.Link>
                    <Nav.Link href=""></Nav.Link>
                    <Nav.Link href=""></Nav.Link>
                </Nav>
                <Form inline classname="justify-content-end">
                    <Button variant={toggle ? "success" : "danger"} onClick={ressolveHandle} >{toggle ? "Ressolved" : "Unressolved"}</Button>
                </Form>
            </Navbar>

            <form action="" className="send-msg" onSubmit={addMessage}>

                <div id="chat-message-list" >
                    {inbox.map((message) => (
                        message.sender === null ?
                            <div className="message-row admin-message" ref={setRef}>
                                <div className="message-content">
                                    <div className="message-text"  >{message.message}</div>
                                    <div className="message-time-admin">{message.createdAt}</div>
                                </div>
                            </div>
                            :
                            message.sender === undefined ?
                                <div className="message-row you-message" ref={setRef}>
                                    <div className="message-content">
                                        <div className="message-text"  >{message.message}</div>
                                        <div className="message-time">{message.createdAt}</div>
                                    </div>
                                </div>
                                :
                                message.sender.type === "user" ?
                                    <div className="message-row you-message" ref={setRef}>
                                        <div className="message-content">
                                            <div className="message-text"  >{message.message}</div>
                                            <div className="message-time">{message.createdAt}</div>
                                        </div>
                                    </div>
                                    : message.sender.type === "contact" &&
                                    <div className="message-row other-message" ref={setRef}>
                                        <div className="message-content" >
                                            {/* <img src={userimg} alt="" /> */}
                                            <div className="message-text" >{message.message}</div>
                                            <div className="message-time-sender">{message.createdAt}</div>
                                        </div>
                                    </div>
                    ))}


                </div>
                <div className="send-bar">

                    <Form.Group>

                        <Form.Control type="text" value={sentMessage} className="send-messages" onChange={(e) => setsentMessage(e.target.value)} />
                        <Button variant="success" type="submit" className="send-btn"><FontAwesomeIcon icon={faPaperPlane} ></FontAwesomeIcon></Button>
                    </Form.Group>
                </div>
            </form>












        </>
    )
}

export default ChatBox
// , sentTime, message_id, inbox_id