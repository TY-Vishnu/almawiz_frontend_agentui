import React, { useContext } from 'react'
import useLocalStorage from '../../hooks/useLocalStorage'

const DetailsContext = React.createContext()

export const useDetails = () => {
    return useContext(DetailsContext)
}

export const DetailsProvider = ({ children }) => {
    const [details, setDetails] = useLocalStorage('details', [])

    const createAgentDetails = (id, name, email, password) => {
        setDetails(prevContacts => {
            console.log(prevContacts);
            return [...prevContacts, { id, name, email, password }]
        })
    }
    return (
        <DetailsContext.Provider value={{ details, createAgentDetails }}>
            {children}
        </DetailsContext.Provider>
    )
}

