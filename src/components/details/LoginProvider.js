import React, { useContext } from 'react'
import useLocalStorage from '../../hooks/useLocalStorage'

const LoginContext = React.createContext()

export const useLogin = () => {
    return useContext(LoginContext)
}


export const LoginProvider = ({ children }) => {
    const [login, setLogin] = useLocalStorage('login', [])

    const createLogin = (email) => {
        setLogin((prevLogin) => {
            return [...prevLogin, { email }]
        })
    }

    return (
        <LoginContext.Provider value={{ login, createLogin }}>
            {children}
        </LoginContext.Provider>
    )
}

