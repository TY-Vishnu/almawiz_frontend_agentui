import React, { useRef, useState } from 'react'
import { Form, Button, Card } from 'react-bootstrap'
import { v4 as uuidV4 } from 'uuid';
import { useDetails } from '../../details/DetailsProvider'
import { useHistory } from "react-router";


// id ? directlogin : createnewid

const SignUp = ({ onIdSubmit, postSignup }) => {
    // const idRef = useRef()
    const nameRef = useRef()
    const emailRef = useRef()
    const passwordRef = useRef()
    const confirmPasswordRef = useRef()
    const { createAgentDetails } = useDetails()

    const [name, setName] = useState(false)
    const [email, setEmail] = useState(false)
    const [password, setPassword] = useState(false)
    const [confirmPassword, setConfirmPassword] = useState(false)


    const history = useHistory();

    const handleSubmit = (e) => {
        e.preventDefault()
        onIdSubmit(uuidV4())
        createAgentDetails(uuidV4, nameRef.current.value, emailRef.current.value, passwordRef.current.value)
        // onIdSubmit(idRef.current.value)
        const checkName = new RegExp("^[a-zA-Z\\s\\.]*$");
        const checkEmail = new RegExp(
            "^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$"
        );
        const checkPassword = new RegExp("^(?=.*)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$")

        const testName = checkName.test(nameRef.current.value);
        const testEmail = checkEmail.test(emailRef.current.value);
        const testPassword = checkPassword.test(passwordRef.current.value);

        (testName === false || nameRef.current.value.length === 0) ? setName(true) : setName(false);

        (testEmail === false) ? setEmail(true) : setEmail(false);

        (testPassword === false) ? setPassword(true) : setPassword(false);

        (confirmPasswordRef.current.value !== passwordRef.current.value) ? setConfirmPassword(true) : setConfirmPassword(false);
        const emailData = {
            email: emailRef.current.value,
            password: passwordRef.current.value
        }
        postSignup(emailData)
        if (testName && testEmail && testPassword && (confirmPasswordRef.current.value === passwordRef.current.value)) {
            history.push({
                pathname: "/dashboard",

            });
        }

    }

    // const createNewId = () => {
    // }


    return (
        <div>

            <Card bg={'light'} text='dark' style={{ width: '100%' }}>
                <Card.Header style={{ background: "#3fa3ff" }}><Card.Title>Registration</Card.Title></Card.Header>
                <Card.Body>

                    <Card.Text>
                        <Form onSubmit={handleSubmit}>
                            <Form.Group controlId="formBasicName">
                                <Form.Label>Agent Name</Form.Label>
                                <Form.Control type="text" placeholder="Agent Full Name" ref={nameRef} required />
                                {name && (<Form.Text style={{ color: 'red', fontSize: 'small' }} >Please Enter Your Name</Form.Text>)}
                            </Form.Group>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control type="email" placeholder="Enter email" ref={emailRef} required />
                                {email && <Form.Text style={{ color: 'red', fontSize: 'small' }}>
                                    Please Enter Your Email
                               </Form.Text>}
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password" ref={passwordRef} required />
                                {password && <Form.Text style={{ color: 'red', fontSize: 'small' }}>
                                    Please Enter Your Password, password must contain atleast one Uppercase and one Numeric Value
                               </Form.Text>}
                            </Form.Group>
                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Confirm Password</Form.Label>
                                <Form.Control type="password" ref={confirmPasswordRef} placeholder="Confirm Password" required />
                                {confirmPassword && <Form.Text style={{ color: 'red', fontSize: 'small' }} >
                                    Password not matching
                               </Form.Text>}
                            </Form.Group>

                        </Form>
                    </Card.Text>
                </Card.Body>
                <Card.Footer className="text-muted ">
                    <Button variant="success" className="float-right" onClick={handleSubmit} type="submit" >
                        Submit
                </Button>


                </Card.Footer>
            </Card>



        </div>
    )
}

export default SignUp
