import React from 'react'
import { useHistory } from 'react-router'
import { Redirect } from 'react-router'


const LogOut = () => {
    const history = useHistory()
    const logout = () => {
        localStorage.clear();
        // history.push({
        //     pathname: "/",

        // });
        window.location.href = '/';
    }

    return (
        <div onClick={logout}>
            logout
        </div>
    )
}

export default LogOut
