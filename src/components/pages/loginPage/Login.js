import React, { useState, useRef } from 'react'
import { Form, Button, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { Redirect, useHistory } from 'react-router'
import { useLogin } from '../../details/LoginProvider'
import axios from 'axios'
import background from './call-center-2006866_1920.png'

const Login = ({ postSignin, accessToken, loggedIn }) => {
    const emailRef = useRef()
    const passwordRef = useRef()
    const [email, setEmail] = useState(false)
    const [password, setPassword] = useState(false)
    const history = useHistory()
    const { createLogin } = useLogin()

    const handleSubmit = (e) => {
        // debugger
        e.preventDefault()
        createLogin(accessToken)
        const checkEmail = new RegExp(
            "^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$"
        );
        const checkPassword = new RegExp("^(?=.*)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$")
        const testEmail = checkEmail.test(emailRef.current.value);
        const testPassword = checkPassword.test(passwordRef.current.value);

        (testEmail === false) ? setEmail(true) : setEmail(false);

        (testPassword === false) ? setPassword(true) : setPassword(false);

        // const requestOptions = {
        //     method: "POST",
        //     headers: { "Content-Type": "application/json" },
        //     body: JSON.stringify({ email: emailRef.current.value, password: passwordRef.current.value })
        // };
        const emailData = {
            email: emailRef.current.value,
            password: passwordRef.current.value
        }
        postSignin(emailData)
        // axios.post('http://chat-server-pritam.herokuapp.com/signin',
        //     emailData
        // )
        //     .then((res) => {
        //         // setChats([...chats, res])
        //         // console.log(newMsg);
        //         // setChats([...chats, newMsg])
        //         // console.log(chats);
        //         console.log(res.data)

        //     })
        //     .catch((err) => {
        //         alert('account does not exist')
        //     })
        // addLoginDetails(emailRef.current.value, passwordRef.current.value)
        // if (testEmail && testPassword && postSignin) {
        //     history.push({
        //         pathname: "/dashboard",

        //     });
        // }
        // history.push({
        //     pathname: "/dashboard",

        // });

    }
    // const addLoginDetails = async (emailRef, passwordRef) => {

    //     const loginData = 

    // }

    if (loggedIn) {
        return <Redirect to={'/dashboard'} />
    }




    return (
        <div style={{
            backgroundImage: `url(${background})`,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            margin: '0px',
            height: '100vh'
        }}>


            <Card
                bg={'light'}
                text={'dark'}
                className='test-center'
                style={{
                    width: '40%',
                    justifyContent: 'center',
                    marginLeft: '33%',
                    boxShadow: '3px 6px 47px -5px rgb(0 0 0 / 26%)',
                }}
            >

                <Card.Header style={{ background: "#3fa3ff" }} as="h5">Login</Card.Header>
                <Card.Body varient='info'>

                    <Card.Text>
                        <Form onSubmit={handleSubmit}>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control type="email" ref={emailRef} placeholder="Enter email" />
                                {email && <Form.Text style={{ color: 'red', fontSize: 'small' }}>
                                    Please Enter Your Email
                               </Form.Text>}
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" ref={passwordRef} placeholder="Password" />
                                {password && <Form.Text style={{ color: 'red', fontSize: 'small' }}>
                                    Please Enter Your Password, password must contain atleast one Uppercase and one Numeric Value
                               </Form.Text>}
                            </Form.Group>
                            <Form.Group controlId="formBasicCheckbox">
                                {/* <Form.Label >New User?
                    <Link to="/signUp"  >Register</Link>
                                </Form.Label> */}
                            </Form.Group>

                        </Form>
                    </Card.Text>
                    {/* <Link to="/dashboard" > */}
                    <Button variant="success" type="submit" className="float-right" onClick={handleSubmit} >
                        Submit
                </Button>
                    {/* </Link> */}
                </Card.Body>
            </Card>



        </div>
    )
}

export default Login
