import './ChatPage.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useState, useRef, useEffect } from 'react'
import ChatBox from '../../chatComponents/chatBox/ChatBox';
import Header from '../../chatComponents/header/Header';
import Buttons from '../../Buttons';
import ChatList from '../../chatComponents/chatList/ChatList';

function ChatPage() {

    const messagesEndRef = useRef(null);
    const [inbox, setInbox] = useState([{}]);
    const [userData, setUserData] = useState('');
    const [user, setUser] = useState([
        {
            id: 1,
            name: 'John Wick',
            message: 'hello how are you doin',
            created: '15th Jan',
            inbox: [{
                message_id: 'agent1',
                sendername: 'John Wick',
                sentMessage: 'Hello how can i help you',
                sentTime: "50:89"
            },
            {
                message_id: 'user1',
                sendername: 'John Wick',
                recievedMessage: 'hello how are you doin',
                recievedTime: "5:12"
            },],
            status: 'Ressolved'
        },
        {
            id: 2,
            name: 'Frank Castle',
            message: 'Feeling Lucky Punk',
            created: '19th Feb',
            inbox: [{
                message_id: 'agent1',
                sendername: 'Frank Castle',
                sentMessage: 'Hello how can i help you',
                sentTime: "50:89"
            },
            {
                message_id: 'user1',
                sendername: 'Frank Castle',
                recievedMessage: 'Feeling Lucky Punk',
                recievedTime: "5:12"
            },],
            status: 'Ressolved'
        },
        {
            id: 3,
            name: 'Matt Murdock',
            message: 'I believe in justice system',
            created: '2 days ago',
            inbox: [{
                message_id: 'agent1',
                sendername: 'Matt Murdock',
                sentMessage: 'Hello how can i help you',
                sentTime: "50:89"
            },
            {
                message_id: 'user1',
                sendername: 'Matt Murdock',
                recievedMessage: 'I believe in justice system',
                recievedTime: "5:12"
            },],
            status: 'Ressolved'
        },
        {
            id: 4,
            name: 'Damon Salvatore',
            message: 'hello brother',
            created: '15th Oct',
            inbox: [{
                message_id: 'agent1',
                sendername: 'Damon Salvatore',
                sentMessage: 'Hello how can i help you',
                sentTime: "50:89"
            },
            {
                message_id: 'user1',
                sendername: 'Damon Salvatore',
                recievedMessage: 'hello brother',
                recievedTime: "5:12"
            },],
            status: 'Unessolved'
        },
        {
            id: 5,
            name: 'Sherlock Holmes',
            message: 'hello how are you doin',
            created: '25th Dec',
            inbox: [{
                message_id: 'agent1',
                sendername: 'Sherlock Holmes',
                sentMessage: 'Hello how can i help you',
                sentTime: "50:89"
            },
            {
                message_id: 'user1',
                sendername: 'Sherlock Holmes',
                recievedMessage: 'hello how are you doin',
                recievedTime: "5:12"
            },],
            status: 'Unressolved'
        },
        {
            id: 6,
            name: 'John Snow',
            message: 'You know nothing john snow',
            created: '15th Jan',
            inbox: [{
                message_id: 'agent1',
                sendername: 'John Snow',
                sentMessage: 'Hello how can i help you',
                sentTime: "50:89"
            },
            {
                message_id: 'user1',
                sendername: 'John Snow',
                recievedMessage: 'You know nothing john snow',
                recievedTime: "5:12"
            },],
            status: 'Unressolved'
        },
        {
            id: 7,
            name: 'Stephen Strange',
            message: 'Pain is an old friend',
            created: '15th Jan',
            inbox: [{
                message_id: 'agent1',
                sendername: 'Stephen Strange',
                sentMessage: 'Pain is an old friend',
                sentTime: "50:89"
            },
            {
                message_id: 'user1',
                sendername: 'Stephen Strange',
                recievedMessage: 'Hello',
                recievedTime: "5:12"
            },],
            status: 'Ressolved'
        }
    ]);


    // const scrollToBottom = () => {
    //   messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
    // };
    // useEffect(scrollToBottom, [inbox]);

    const addNewMessage = (sentMessage, sentTime) => {
        const message_id = "agent1"
        const newMessages = { message_id, ...sentMessage, ...sentTime }
        setInbox([...inbox, newMessages])
        console.log('new messages', newMessages);
        console.log('sent messages', sentMessage, 'message id', message_id);
    }

    const handleInbox = (id) => {
        const inboxData = user.filter((userChat) => userChat.id === id)
        setInbox(inboxData[0].inbox)
        const userData = user.filter((userChat) => userChat.id === id)
        setUserData(userData[0].name)
        // console.log(inbox);
        console.log(userData);
    }



    return (
        <div class="grid">
            <aside class="sidebar-left">
                <Header name="Agent" />
                <Buttons />
                {/* <ChatList userChats={user} onClick={handleInbox} />  */}
                {user.length > 0 ? <ChatList userChats={user} onClick={handleInbox} /> : <h5 style={{ textAlign: 'center' }}>No Chats To display</h5>}
            </aside>

            <article>
                <ChatBox inbox={inbox} userData={userData} onAdd={addNewMessage} scrollBottom={messagesEndRef} />
            </article>

            <aside class="sidebar-right">
                {/* {inbox.map((item) => (item.message_id === "agent1" ? <h5>{item.sentMessage}</h5> : <h5>{item.recievedMessage}</h5>))} */}
                {/* <div>{userData}</div> */}
        agent details
  </aside>


        </div>
    );
}

export default ChatPage;
