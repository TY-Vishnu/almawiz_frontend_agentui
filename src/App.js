import './App.css';
import Header from './components/chatComponents/header/Header';
import 'bootstrap/dist/css/bootstrap.min.css';
import Sidebar from './components/chatComponents/sideBar/Sidebar';
import ChatList from './components/chatComponents/chatList/ChatList';
import { useState, useRef, useEffect } from 'react'
import ChatBox from './components/chatComponents/chatBox/ChatBox';
import AddProp from './components/chatComponents/agentDetails/AgentProp';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from './components/pages/loginPage/Login';
import SignUp from './components/pages/signUpPage/SignUp';
import { serverToAgent } from './api';
import useLocalStorage from './hooks/useLocalStorage';
import { DetailsProvider } from './components/details/DetailsProvider'
import axios from 'axios';
import Chats from './components/chatComponents/chatList/Chats';
import { Spinner, Button } from 'react-bootstrap'

function App() {


  const [id, setId] = useState('id');
  const messagesEndRef = useRef(null);
  const [inbox, setInbox] = useState([{}]);
  const [inboxId, setInboxId] = useState(1);
  const [userData, setUserData] = useState('');
  const [conversationsId, setconversationsId] = useState()
  const [data, setData] = useState([])
  const [unRessolvedData, setUnRessolvedData] = useState([])
  const [ressolvedData, setRessolvedData] = useState([])
  const [chats, setChats] = useState([])
  const [loginData, setLoginData] = useState({})
  const [loggedIn, setLoggedIn] = useState(false)
  const [agentId, setAgentId] = useState()
  const [showChat, setShowChat] = useState(false)
  const [chatLoading, setChatLoading] = useState(false)
  const [allData, setAllData] = useState([])
  const [accessToken, setAccessToken] = useLocalStorage('', [])
  const [accessNewToken, setAccessNewToken] = useState()
  const [unresolvedLoading, setUnresolvedLoading] = useState(false)
  const [resolvedLoading, setResolvedLoading] = useState(false)
  const [showDashboard, setShowDashboard] = useState(false)
  // const CONVERSATION_ID = 11;
  const [conversations, setConversations] = useState([

  ]);
  console.log("agent id", loginData.id);

  // useEffect(() => {
  //   const getData = async () => {
  //     const dataFromServer = await fetchData()
  //     conversations(dataFromServer)
  //   }
  //   getData()
  // }, [])

  const signUp = (
    <DetailsProvider>
      <SignUp onIdSubmit={setId} />
    </DetailsProvider>
  )

  const agentData = (
    <DetailsProvider>
      <AddProp userData={userData} />
    </DetailsProvider>
  )

  // const AGENT_ID = agentId; //6414 
  // useEffect(() => {

  //   const LoadAllChatList = setInterval(() => {
  //     if (agentId) {

  //       axios.post('http://chat-server-pritam.herokuapp.com/account/conversations',
  //         {
  //           agentid: agentId,

  //         }
  //       )
  //         .then((res) => {
  //           // debugger
  //           // res = JSON.parse(data);
  //           setData(res.data.conversation)
  //           console.log(res.data.conversation)
  //           // console.log(data.data.conversation[0].sender.name);
  //         })
  //         .catch((err) => {
  //           console.log(err);
  //         })
  //     }
  //     return clearInterval(LoadAllChatList)
  //   }, 1000)
  // }, [])


  // const fetchChatList = () => {
  //   // debugger

  //   console.log('fetchChatList')

  //   axios.post('http://chat-server-pritam.herokuapp.com/account/conversations',
  //     {
  //       agentid: agentId,

  //     }
  //   )
  //     .then((res) => {

  //       setData(res.data.conversation)
  //       console.log(res.data.conversation)
  //       console.log(data.data.conversation[0].sender.name);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     })
  // }




  //load all unressolved chat list 
  // useEffect(() => {
  //   const loadAllUnressolved = setInterval(() => {
  //     // debugger
  //     if (agentId) {
  //       // debugger
  //       axios.get(`https://chat-server-pritam.herokuapp.com/account/${agentId}/conversations?status=open`)
  //         .then(res => {
  //           // debugger
  //           console.log('unressolved', res.data.data);
  //           setUnRessolvedData(res.data.data)
  //           setUnresolvedLoading(true)
  //         }).catch(err => {
  //           console.log(err);
  //         })
  //       return clearInterval(loadAllUnressolved)
  //     }
  //   }, 5000)
  // })

  //load all ressolved chat list 
  useEffect(() => {
    const loadAllRessolved = setInterval(() => {
      // debugger
      if (agentId) {
        // debugger
        axios.get(`https://chat-server-pritam.herokuapp.com/account/${agentId}/conversations?status=resolved`)
          .then(res => {
            // debugger
            console.log('ressolved', res.data.data);
            setRessolvedData(res.data.data)
            setResolvedLoading(true)
          }).catch(err => {
            console.log(err);
          })
        axios.get(`https://chat-server-pritam.herokuapp.com/account/${agentId}/conversations?status=all`)
          .then(res => {
            // debugger
            console.log('all', res.data.data);
            setAllData(res.data.data)
            setChatLoading(true)
          }).catch(err => {
            console.log(err);
          })
        axios.get(`https://chat-server-pritam.herokuapp.com/account/${agentId}/conversations?status=open`)
          .then(res => {
            // debugger
            console.log('unressolved', res.data.data);
            setUnRessolvedData(res.data.data)
            // setUnresolvedLoading(true)
          }).catch(err => {
            console.log(err);
          })
        return clearInterval(loadAllRessolved)
      }
    }, 5000)
  })

  //load all 
  useEffect(() => {
    const loadAllRessolved = setInterval(() => {
      // debugger
      if (agentId) {
        // debugger

        return clearInterval(loadAllRessolved)
      }
    }, 5000)
  })

  if (chatLoading && resolvedLoading && unresolvedLoading) {
    setShowDashboard(true)
  }





  // useEffect(() => {

  useEffect(() => {
    const loadAllConversation = setInterval(() => {
      // debugger

      console.log("chat bar", agentId)

      const convoDetails = {
        agentid: agentId,
        conversationid: conversationsId,
      }
      if (agentId) {

        axios.post('http://chat-server-pritam.herokuapp.com/account/conversations/messages',
          convoDetails
        )
          .then((res) => {
            // res = JSON.parse(data);
            setChats(res.data.data)
            console.log(res.data.data)
            // console.log(data.data.conversation[0].sender.name);
          })
          .catch((err) => {
            console.log(err);
          })
      }


      return clearInterval(loadAllConversation)
    }, 1000)

  })
  // })
  // setInterval(async () => {
  //   await axios
  //     .post(
  //       'http://chat-server-pritam.herokuapp.com/account/conversations',
  //       {
  //         agentid: 6414,

  //       }
  //     )
  //     .then((data) => {
  //       // data = JSON.parse(data);
  //       setData(data.data)
  //       console.log(data)
  //       console.log(data.data.conversation[0].sender.name);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // }, 5000);
  //fetch data 
  const fetchData = async () => {
    const res = await fetch('http://chat-server-pritam.herokuapp.com/account/conversations', {
      method: 'POST',
      headers: { 'content-Type': 'application/json' },
      body: JSON.stringify({
        'agentid': 6414
      })
    })
    const data = await res.json()
    console.log('data', data);
    return data
  }

  //socket io
  // serverToAgent((err, sentMessage) => setConversations({
  //   sentMessage
  // }));

  // const scrollToBottom = () => {
  //   messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  // };
  // useEffect(scrollToBottom, [inbox]);






  const addNewMessage = async (newMsg) => {
    // debugger
    const sendingDetails = {
      conversationid: conversationsId,
      type: "outgoing",
      message: newMsg

    }


    await axios.post('http://chat-server-pritam.herokuapp.com/sendMessage',
      sendingDetails
    )
      .then((res) => {
        // debugger
        // setChats([...chats, res])
        console.log(newMsg);
        setChats([...chats, newMsg])
        console.log(chats);
        console.log(res)

      })
      .catch((err) => {
        console.log(err);
      })










    // const res = await fetch('http://localhost:5005/user', {
    //   method: 'POST',
    //   headers: { 'Content-Type': 'application/json' },
    //   body: JSON.stringify(newMsg)
    // })
    // const data = await res.json()
    // console.log(newMsg);
    // setChats([...chats, newMsg])
    // console.log(chats);







    // const message_id = "agent1"
    // const inbox_id = Math.floor(Math.random() * 10000) + 1
    // const newMessages = { message_id, inbox_id, ...sentMessage, ...sentTime }
    // setInbox([...inbox, newMessages])
    // console.log('new messages', newMessages);
    // console.log('sent messages', sentMessage, 'message id', message_id);
  }

  const selectChat = (id) => {
    setconversationsId(id)
    console.log(id);
    setShowChat(true);
    const selected = data.map(select => select.conversation.id === id && select.conversation.assignee)
    console.log('selected', selected)
    // const selected = data.conversation.id === id
    // console.log('selected', selected);
    // const userName = chats.filter((name) => name.conversation.id === id)
    // console.log("username", userName);
  }
  // useEffect(() => {

  const postSignIn = (emailData) => {
    axios.post('http://chat-server-pritam.herokuapp.com/signin',
      {
        email: emailData.email,
        password: emailData.password
      }
    )
      .then((res) => {
        // debugger
        const { user } = res.data
        setLoginData(user)
        setAgentId(user.id)
        setAccessToken(user.access_token)
        setAccessNewToken(user.access_token)
        localStorage.setItem('token', res.data.token)
        setLoggedIn(true)

        console.log(res)


      })
      .catch((err) => {
        alert('account does not exist')
      })
  }



  // useEffect(() => {
  //   if (agentId !== undefined) {

  //     fetchChatList()
  //   }
  // }, [agentId])

  const handleInbox = (id) => {
    const inboxData = conversations.filter((userChat) => userChat.id === id)
    setInboxId(inboxData[0].inbox[0].inbox_id)
    setInbox(inboxData[0].inbox)
    const userData = conversations.filter((userChat) => userChat.id === id)
    setUserData(userData[0].name)
    // console.log(inbox);
    console.log(inboxId);
  }

  const findName = (conversationsId) => {
    for (var i = 0; i < data.length; i++) {
      if (data[i].conversation.id === conversationsId) {
        return data[i].sender.name
      }
    }
  }

  console.log(findName(conversationsId));


  // const result = data
  //   .filter((item) => item.conversation.idincludes(conversationsId))
  // // .map(name => name.sender.name);
  // console.log(result);

  return (
    <Router>
      <Route
        path='/dashboard'
        exact
        render={(props) => (


          <div>
            {  resolvedLoading & chatLoading ?
              <div class="grid">
                <aside class="sidebar-left">
                  {/* <h1>{agentId}</h1> */}

                  <Header agentData={loginData} agentId={agentId} />
                  <Sidebar userChats={data} onClick={selectChat} resolvedLoading={resolvedLoading} unresolvedLoading={resolvedLoading} chatLoading={chatLoading} conversationsId={conversationsId} allData={allData} unRessolvedData={unRessolvedData} ressolvedData={ressolvedData} />
                  {/* {user.length > 0 ? <ChatList userChats={user} onClick={handleInbox} /> : <h5 style={{ textAlign: 'center' }}>No Chats To display</h5>} */}
                </aside>
                <Switch>
                  <article>
                    {/* <div>{conversationsId}</div> */}
                    {/* {inbox.length > 1 ? <Route path='/conversations/:conversationId' component={Chats} /> : <h5 style={{ textAlign: 'center' }}>select the Chats To display</h5>} */}
                    {/* <Route path='/conversations/:conversationId'><ChatBox inbox={chats} inboxId={inboxId} userData={userData} onAdd={addNewMessage} scrollBottom={messagesEndRef} /></Route> */}
                    {showChat ? <ChatBox ressolvedData={ressolvedData} inbox={chats} conversationsId={conversationsId} allData={allData} data={data} onAdd={addNewMessage} agentId={agentId} scrollBottom={messagesEndRef} /> : <h5 className="before-box">Please select a conversation from left pane</h5>}
                    {/* <ChatBox inbox={chats} userData={userData} onAdd={addNewMessage} scrollBottom={messagesEndRef} /> */}
                    {/* {chats.map((chat) => (
              
            chat.sender === null ? <h5>{chat.message}<h6>{chat.createdAt}</h6></h5> : chat.sender.type === 'user' && <h5>{chat.message}<h6>{chat.createdAt}</h6></h5>))} */}
                  </article>
                </Switch>
                <aside class="sidebar-right">
                  {/* {data.filter((convo, conversationId) => convo.conversation.id === conversationId && (<div >{convo.sender.name}</div>))} */}
                  {/* {inbox.map((item) => (item.message_id === "agent1" ? <h5>{item.sentMessage}</h5> : <h5>{item.recievedMessage}</h5>))} */}
                  {/* <div>{inboxId}</div> */}
                  {/* <Col xs={6} md={4}>
      <Image src={agentImg} roundedCircle />
      
    </Col> */}
                  {/* <h1>{selected }</h1> */}
                  {/* {ressolvedData.map((resolved => resolved.sender.name))} */}
                  <AddProp />


                </aside>
              </div> : <Button variant="primary" style={{
                width: '100%',
                height: '100vh'
              }} disabled>
                <Spinner
                  as="span"
                  animation="grow"
                  size="lg"
                  role="status"
                  aria-hidden="true"
                />
Loading...
</Button>}
          </div>
        )}
      />



      {/* <Route path='/signUp' exact
        render={() => (
          signUp
        )}
      /> */}
      <Route path='/' exact
        render={(props) => (
          <div>
            <Login postSignin={postSignIn} loggedIn={loggedIn} accessToken={accessToken} />
          </div>
        )} />
    </Router>
  );
}

export default App;